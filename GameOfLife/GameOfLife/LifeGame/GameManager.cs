﻿using System;
using System.Collections.Concurrent;
using GameOfLife.LifeGame.instance;
using Microsoft.AspNet.SignalR;


namespace GameOfLife.LifeGame
{
    /// <summary>
    /// This manager class is going to behave as the layer that allows us to have 
    /// many games running at once.  
    /// 
    /// 
    /// It should not have any game logic in it.
    /// </summary>
    public class GameManager
    {
        private static readonly Lazy<GameManager> _instance = new Lazy<GameManager>();
        public static GameManager Instance { get { return _instance.Value; } }

        private readonly ConcurrentDictionary<Guid, GameOfLifeInstance> _Games = new ConcurrentDictionary<Guid, GameOfLifeInstance>();

        public bool JoinGame(out string failureMessage, Guid gameId, string playerAddress)
        {
            GameOfLifeInstance gi;
            if (!_Games.TryGetValue(gameId, out gi))
            {
                failureMessage = "For some reason you can't join this game";
                return false;
            }
            lock (gi)
            {
                if (!gi.AddPlayer(out failureMessage, playerAddress, PlayerType.Human))
                    return false;
                return true;
            }
        }

        public bool StartGame(out string failureMessage, Guid gameId, string playerId)
        {
            GameOfLifeInstance gi;
            if (!_Games.TryGetValue(gameId, out gi))
            {
                failureMessage = "Can't Start a game that doesn't exit";
                return false;
            }
            lock (gi)
            {
                if (!gi.StartGame(out failureMessage, playerId))
                    return false;
                return true;
            }

        }

        public bool Spin(out string failureMessage, Guid gameId, string playerId, out int spinningTime, out int result)
        {
            failureMessage = null;
            spinningTime = 0;
            result = 0;
            GameOfLifeInstance gi;
            if (!_Games.TryGetValue(gameId, out gi))
            {
                return false;
            }
            lock (gi)
            {
                try
                {
                    gi.SendMessageToClient += gi_SendMessageToClient;
                    if (!gi.Spin(out failureMessage, playerId, out spinningTime, out result))
                    {
                        return false;
                    }
                    return true;
                }
                finally
                {
                    gi.SendMessageToClient -= gi_SendMessageToClient;
                }
            }

        }

        void gi_SendMessageToClient(object source, MessageToClientArgs e)
        {
            var hub = GlobalHost.ConnectionManager.GetHubContext<LifeHub>();

            if (e.MessageType == ClientMessageType.Question)
            {
                hub.Clients.Client(e.PlayerId).Player_Question(e.QuestionText);
            }

            if (e.MessageType == ClientMessageType.PlayerUpdate)
            {
                var p = e.PlayerInfo;
                hub.Clients.Group(e.gameId.ToString()).Game_Player_Update(
                    p.PlayerAddress, p.CurrentTile, p.playerid,
                    p.BankBalance, p.HasAutoInsurance, p.HasFireInsurance);
            }
        }

        public bool CreateGame(out string failureMessage, out Guid gameId)
        {
            var gm = Instance;
            failureMessage = string.Empty;
            lock (gm)
            {
                gameId = Guid.Empty;
                if (_Games.Count > 200)
                {
                    failureMessage = "Too many games currently running";
                    return false;
                }
                var goi = new GameOfLifeInstance();
                gameId = goi.Gameid;
                if (_Games.TryAdd(gameId, goi))
                    return true;
                failureMessage = "Failed to add new game to collection";
                return false;
            }
        }

        public bool QuestionResponse(out string failureMessage, Guid gameId, string playerId, string response)
        {
            failureMessage = null;
            GameOfLifeInstance gi;
            if (!_Games.TryGetValue(gameId, out gi))
            {
                return false;
            }
            lock (gi)
            {
                try
                {
                    gi.SendMessageToClient += gi_SendMessageToClient;
                    return gi.QuestionResponse(out failureMessage, playerId, response);
                }
                finally
                {
                    gi.SendMessageToClient -= gi_SendMessageToClient;
                }

            }
        }
    }
}