using System;
using GameOfLife.LifeGame.instance;

namespace GameOfLife.LifeGame
{
    public class GameTileActionArgs : EventArgs { }
    public class GameTileQuestionArgs : GameTileActionArgs
    {
        public string QuestionText { get; set; }
        public Question QuestionType { get; set; }
    }

    public class SpinAgainArgs : GameTileActionArgs { }

    public class SpinForGiftsArgs : GameTileActionArgs { }

    public class PlayerBalanceChangeArgs : GameTileActionArgs
    {
        public int Amount { get; set; }
        public string FlavorText { get; set; }
    }

    public class GoToTileArgs : GameTileActionArgs
    {
        public int tile { get; set; }
    }
    public delegate void GameTileActionHandler(Player currentPlayer, GameTileActionArgs e);


    public class MessageToClientArgs : EventArgs
    {
        public Guid gameId;
        public string QuestionText { get; set; }
        public ClientMessageType MessageType { get; set; }
        public string PlayerId { get; set; }
        public Player PlayerInfo { get; set; }
    }
    public delegate void NewMessageToClientHandler(object source, MessageToClientArgs e);

    public delegate void PlayerUpdateOnClientHandler(Player currentPlayer, EventArgs e);
}