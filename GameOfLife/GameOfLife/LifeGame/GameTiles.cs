﻿namespace GameOfLife.LifeGame
{
    public class GameTiles
    {
        //I made the arrays 1 bit larger because I wanted to reference everything in 1 based array's rather than 0 based
        public readonly bool[] _occupiedTiles = new bool[163];
        readonly TileColor[] _tileColors = new TileColor[163];
        public readonly bool[] _decesionTiles = new bool[163];

        public event GameTileActionHandler TileAction;
        public GameTiles()
        {
            for (var x = 0; x < _occupiedTiles.Length; x++)
                _occupiedTiles[x] = false;

            for (var x = 0; x < _tileColors.Length; x++)
                _tileColors[x] = TileColor.Yellow;

            for (var x = 0; x < _decesionTiles.Length; x++)
                _decesionTiles[x] = false;

            _decesionTiles[3] = true;
            _decesionTiles[54] = true;
            _decesionTiles[90] = true;

            //setup the white tiles
            var whiteTiles = new[] { 1 };
            foreach (var x in whiteTiles)
                _tileColors[x] = TileColor.White;

            //setup the red tiles
            var redTiles = new[] { 5, 6, 15, 17, 20 };
            foreach (var x in redTiles)
                _tileColors[x] = TileColor.Red;

            //setup the gold tiles
            var goldTiles = new int[] { };
            foreach (var x in goldTiles)
                _tileColors[x] = TileColor.Gold;

        }

        public bool DoActionCheck(int tileId)
        {
            return _decesionTiles[tileId]
                || (_tileColors[tileId] == TileColor.Red || _tileColors[tileId] == TileColor.White);
        }

        public void DoAction(int currentTile, ref Player currentPlayer, bool moveComplete)
        {
            switch (currentTile)
            {
                case 1:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = 10000 });
                    TileAction(currentPlayer, new GameTileQuestionArgs { QuestionType = Question.AutoInsurance });
                    break;
                case 2:
                    TileAction(currentPlayer, new SpinAgainArgs());
                    break;
                case 3:
                    //Decide "Go College or Go Business"
                    TileAction(currentPlayer, new GameTileQuestionArgs { QuestionType = Question.CollegeOrBusiness });
                    if (moveComplete)
                        TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = 4000 });
                    break;
                case 25:
                    TileAction(currentPlayer, new GameTileQuestionArgs { QuestionType = Question.LifeInsurance });
                    break;
                case 42:
                    TileAction(currentPlayer, new GameTileQuestionArgs { QuestionType = Question.FireInsurance });
                    break;
                case 53:
                    TileAction(currentPlayer, new GameTileQuestionArgs { QuestionType = Question.BuyStock });
                    break;

                //Lose Turn
                case 7:
                case 32:
                case 40:
                    currentPlayer.LoseNextTurn = true;
                    break;

                //Get Income assigned
                case 10:
                    currentPlayer.Income = 50000;
                    break;
                case 11:
                    currentPlayer.Income = 24000;
                    break;
                case 12:
                    currentPlayer.Income = 50000;
                    break;
                case 13:
                    currentPlayer.Income = 20000;
                    break;
                case 14:
                    currentPlayer.Income = 30000;
                    break;
                case 15:
                    currentPlayer.Income = 16000;
                    break;
                case 17:
                    if (!currentPlayer._returnedToStart)
                        currentPlayer.Income = 12000;
                    break;
                case 30:
                    TileAction(currentPlayer, new SpinForGiftsArgs());
                    TileAction(currentPlayer, new SpinAgainArgs());
                    break;

                //Collect / Lose money
                case 16:
                case 19:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -1000 });
                    break;
                case 21:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = 1000 });
                    break;
                case 4:
                case 9:
                case 29:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -2000 });
                    break;
                case 8:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = 2000 });
                    break;
                case 31:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -3000 });
                    break;
                case 5:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = 3000 });
                    break;
                case 6:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -5000 });
                    break;
                case 33:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -6000 });
                    break;
                case 34:
                    if (!currentPlayer.HasAutoInsurance)
                        TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -12000 });
                    break;
                case 18:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = 12000 });
                    break;
                case 38:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -20000 });
                    break;
                case 23:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -30000 });
                    break;
                case 41:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -40000 });
                    break;
                case 35:
                case 36:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -60000 });
                    break;
                case 39:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = 96000 });
                    break;
                case 27:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = 120000 });
                    break;
                case 22:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = 240000 });
                    break;

                case 24:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = 300000 });
                    break;

                case 46: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -20000 }); break;
                case 48: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +300000 }); break;
                case 57: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +280000 }); break;
                case 62: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +480000 }); break;
                case 65: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +100000 }); break;
                case 67: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -16000 }); break;
                case 68: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +50000 }); break;
                case 69: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -150000 }); break;
                case 75: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -120000 }); break;
                case 80: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +120000 }); break;
                case 84: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -240000 }); break;
                case 89: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +100000 }); break;
                case 94: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +24000 }); break;
                case 95: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -100000 }); break;
                case 101: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +240000 }); break;
                case 105: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -20000 }); break;
                case 107: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -10000 }); break;
                case 109: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -20000 }); break;
                case 111: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -2000 }); break;
                case 112: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -140000 }); break;
                case 113: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +96000 }); break;
                case 114: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -20000 }); break;
                case 115: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -100000 }); break;
                case 116: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -3000 }); break;
                case 119: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -80000 }); break;
                case 121: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +12000 }); break;
                case 123: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -10000 }); break;
                case 127: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -120000 }); break;
                case 128: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -2000 }); break;
                case 132: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +240000 }); break;
                case 133: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +10000 }); break;
                case 135: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -40000 }); break;
                case 136: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +120000 }); break;
                case 140: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +120000 }); break;
                case 141: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -50000 }); break;
                case 142: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +200000 }); break;
                case 144: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -10000 }); break;
                case 146: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +240000 }); break;
                case 147: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +3000 }); break;
                case 148: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -70000 }); break;
                case 150: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +480000 }); break;
                case 153: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -20000 }); break;
                case 155: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -40000 }); break;
                case 156: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -70000 }); break;
                case 161: TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -20000 }); break;

                case 58: if (currentPlayer.HasStock) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -22000 }); break;
                case 63: if (currentPlayer.HasStock) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -36000 }); break;
                case 74: if (currentPlayer.HasStock) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +240000 }); break;
                case 76: if (currentPlayer.HasStock) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -16000 }); break;
                case 77: if (currentPlayer.HasStock) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +120000 }); break;
                case 93: if (currentPlayer.HasStock) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +12000 }); break;
                case 100: if (currentPlayer.HasStock) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +18000 }); break;
                case 106: if (currentPlayer.HasStock) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +360000 }); break;
                case 126: if (currentPlayer.HasStock) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +140000 }); break;
                case 138: if (currentPlayer.HasStock) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +480000 }); break;
                case 159: if (currentPlayer.HasStock) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +600000 }); break;

                case 79: if (currentPlayer.HasLifeInsurance) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +70000 }); break;
                case 97: if (currentPlayer.HasLifeInsurance) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +60000 }); break;
                case 125: if (currentPlayer.HasLifeInsurance) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +120000 }); break;
                case 149: if (currentPlayer.HasLifeInsurance) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +240000 }); break;
                case 160: if (currentPlayer.HasLifeInsurance) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = +240000 }); break;

                case 98: if (!currentPlayer.HasAutoInsurance) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -50000 }); break;
                case 99: if (!currentPlayer.HasFireInsurance) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -60000 }); break;
                case 143: if (currentPlayer.HasFireInsurance) TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = -100000 }); break;





                //Paydays
                case 20:
                case 28:
                case 37:
                case 43:
                case 49:
                case 59:
                case 64:
                case 73:
                case 78:
                case 85:
                case 92:
                case 96:
                case 102:
                case 110:
                case 117:
                case 124:
                case 131:
                case 139:
                case 145:
                case 154:
                    TileAction(currentPlayer, new PlayerBalanceChangeArgs { Amount = currentPlayer.Income });
                    break;



            }
        }
    }
}
