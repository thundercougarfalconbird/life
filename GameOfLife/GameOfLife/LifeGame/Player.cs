﻿using System.Collections.Generic;

namespace GameOfLife.LifeGame
{
    public class Player
    {

        public string PlayerAddress { get; private set; }
        public PlayerType PlayerType { get; set; }
        public int CurrentTile { get; set; }
        public bool IsTurn { get; set; }
        public int BankBalance { get; set; }
        public bool LoseNextTurn { get; set; }
        public int Income { get; set; }

        public int playerid { get; set; }

        public bool HasAutoInsurance { get; set; }
        public bool HasFireInsurance { get; set; }
        public bool HasLifeInsurance { get; set; }
        public bool HasStock { get; set; }

        public List<Sex> Children = new List<Sex>();
        public bool? route_College = null;
        public bool _returnedToStart;

        public Player(string playerAddress)
        {
            PlayerAddress = playerAddress;
        }

    }

    public enum Sex
    {
        Male,
        Female
    }
}