﻿namespace GameOfLife.LifeGame
{
    public enum Question
    {
        AutoInsurance,
        BuyStock,
        PlayStockMarket,
        CollegeOrBusiness,
        LifeInsurance,
        FireInsurance
    }
}