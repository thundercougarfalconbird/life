﻿namespace GameOfLife.LifeGame
{
    public enum TileColor
    {
        Red,
        Yellow,
        Gold,
        White
    }
}