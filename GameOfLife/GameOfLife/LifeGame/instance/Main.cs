﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace GameOfLife.LifeGame.instance
{
    public partial class GameOfLifeInstance
    {
        private static readonly Random Random = new Random();

        public Guid Gameid { get; private set; }

        private readonly GameTiles _tiles = new GameTiles();
        private readonly ConcurrentDictionary<string, Player> _myPlayers = new ConcurrentDictionary<string, Player>();

        public GameOfLifeInstance()
        {
            Gameid = Guid.NewGuid();
        }

        public bool Spin(out string failureMessage, string playerId, out int spinningTime, out int result)
        {
            spinningTime = 0;
            result = 0;
            failureMessage = string.Empty;

            if (!GameStartCheck()) { failureMessage = "Game not started, you cannot spin yet"; return false; }
            if (MoveCurrentlyInProgress) { failureMessage = "There is currenlty a move happening already"; return false; }
            if (!_myPlayers[playerId].IsTurn) { failureMessage = "Not this players turn"; return false; }

            spinningTime = Random.Next(1, 5);
            result = Random.Next(1, 10);
            //Thought:  It would be great if here I could send a message to everyone without having to return...
            //Thought:  Since I can't just send a informational message, this will look strange to the user, even though the behavior in the game is correct.
            //          The user will spin, they could then get a question (insurance, turn left or right...) then they would see the spinning results

            var player = _myPlayers[playerId];
            MoveAPlayer_Setup(result, player);
            while (!_waitingForPlayerResponse.HasValue && !MovePlayer(playerId)) { }
            return true;
        }

        public bool NextPlayersTurn()
        {
            var currentPlayerId = (from a in _myPlayers.Values
                                   where a.IsTurn
                                   select a.PlayerAddress).First();
            _myPlayers[currentPlayerId].IsTurn = false;

            var playerOrder = _myPlayers.Keys.OrderBy(a => a).ToList();
            var nextPlayerIndex = -1;
            for (var x = 0; x < playerOrder.Count; x++)
                if (playerOrder[x] == currentPlayerId)
                    nextPlayerIndex = x + 1;
            if (nextPlayerIndex == -1 || nextPlayerIndex >= playerOrder.Count)
                nextPlayerIndex = 0;
            _myPlayers[playerOrder[nextPlayerIndex]].IsTurn = true;
            if (_myPlayers[playerOrder[nextPlayerIndex]].LoseNextTurn)
            {
                _myPlayers[playerOrder[nextPlayerIndex]].LoseNextTurn = false;
                NextPlayersTurn();
            }
            return true;
        }
    }

}