﻿namespace GameOfLife.LifeGame.instance
{
    public partial class GameOfLifeInstance
    {
        private int _numberOfSpacesToMove;
        private int _currentSpacesMoved;
        private int _currentNewTile;
        private Question? _waitingForPlayerResponse;
        private bool MoveCurrentlyInProgress;
        private bool spinAgain;

        private void MoveAPlayer_Setup(int numberOfSpacesToMove, Player currentPlayer)
        {
            _numberOfSpacesToMove = numberOfSpacesToMove;
            _currentSpacesMoved = 0;

            if (currentPlayer.CurrentTile <= 0)
            {
                _numberOfSpacesToMove += 1;
            }
            _currentNewTile = currentPlayer.CurrentTile;
        }

        private bool MovePlayer(string currentPlayerId)
        {
            //Can't move a player until they respond
            if (_waitingForPlayerResponse.HasValue) return false;

            //"Move" the player one space
            var currentPlayer = _myPlayers[currentPlayerId];
            _currentSpacesMoved++;
            _currentNewTile += 1;

            //If the previous space was a direction change tile, make sure we are going in the right direction
            if ((_currentNewTile - 1) == 3)
            {
                if (!currentPlayer.route_College.GetValueOrDefault(false) || currentPlayer._returnedToStart)
                    _currentNewTile = 16;
            }

            //when on the college route you will never hit tile 16 but instead be on tile 20
            if (_currentNewTile == 16 && currentPlayer.route_College.GetValueOrDefault(false))
                _currentNewTile = 20;

            //Are we continuing to move
            MoveCurrentlyInProgress = _numberOfSpacesToMove > _currentSpacesMoved || _tiles._occupiedTiles[_currentNewTile];

            //will this be the last move
            var finalMove = _numberOfSpacesToMove <= _currentSpacesMoved && !_tiles._occupiedTiles[_currentNewTile];
            if (finalMove && _currentNewTile == 26)
            {
                //TODO:  Go back to start, use must go business route but not get new career
                _numberOfSpacesToMove = 0;
                _currentNewTile = 1;
                currentPlayer._returnedToStart = true;
            }
            if (_currentNewTile == 30)
            {
                //Get married
                _numberOfSpacesToMove = 0;
            }
            //if this is NOT the last move, there may be a tile action to do anyways
            if (!finalMove && _tiles.DoActionCheck(_currentNewTile))
            {
                //Do the move as if we are on on the tile
                _tiles.TileAction += HandleTileActions;
                _tiles.DoAction(_currentNewTile, ref currentPlayer, false);
                _tiles.TileAction -= HandleTileActions;
                return false;
            }
            //If not final move, we are done with this function
            if (!finalMove)
                return false;

            //Do the move as if we are on the tile.
            _tiles.TileAction += HandleTileActions;
            _tiles.DoAction(_currentNewTile, ref currentPlayer, true);
            _tiles.TileAction -= HandleTileActions;

            //tiles 10-15 are career tiles and move you to tile 20
            if (_currentNewTile >= 10 && _currentNewTile <= 15)
            {
                _currentNewTile = 20;
                MovePlayer(currentPlayerId);
                return true;
            }

            //update the player and board postions
            _tiles._occupiedTiles[currentPlayer.CurrentTile] = false;
            currentPlayer.CurrentTile = _currentNewTile;
            _tiles._occupiedTiles[currentPlayer.CurrentTile] = true;

            //Update the client(s)
            HandleTileActions(currentPlayer, new GameTileActionArgs());

            //If the player gets to spin again, start the process over again, do not go to the next player.
            if (spinAgain)
            {
                spinAgain = false;
                string failureMessage;
                int spinTime, result;
                Spin(out failureMessage, currentPlayer.PlayerAddress, out spinTime, out result);
            }
            else
                NextPlayersTurn();

            return true; //Moving the player is complete
        }

    }

    public enum ClientMessageType
    {
        Question = 1,
        PlayerUpdate
    }
}