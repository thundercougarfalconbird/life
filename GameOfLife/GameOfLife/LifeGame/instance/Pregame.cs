﻿using System.Linq;
using System.Threading;

namespace GameOfLife.LifeGame.instance
{
    public partial class GameOfLifeInstance
    {
        private bool _gameStarted;

        public bool AddPlayer(out string failureMessage, string playerId, PlayerType playerType)
        {
            failureMessage = string.Empty;
            if (GameStartCheck()) { failureMessage = "Game already started.  You can't join this game"; return false; }
            if (_myPlayers.Count >= 6) { failureMessage = "Already 6 players"; return false; }
            if (_myPlayers.ContainsKey(playerId)) { failureMessage = "You are already in this game"; return false; }

            var p = new Player(playerId) { PlayerType = playerType };
            return _myPlayers.TryAdd(playerId, p);
        }

        public bool StartGame(out string failureMessage, string PlayerId)
        {
            failureMessage = null;

            if (_myPlayers.Count < 2)
            {
                failureMessage = "There must be atleast 2 players to start the game.";
                return false;
            }

            (from a in _myPlayers
             select a.Value)
                .Skip(Random.Next(0, _myPlayers.Count))
                .First().IsTurn = true;

            int count = 1;
            foreach (var a in _myPlayers)
                a.Value.playerid = count++;

            _gameStarted = true;
            return _gameStarted;
        }
        private bool GameStartCheck()
        {
            return _gameStarted;
        }

    }
}