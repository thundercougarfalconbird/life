﻿namespace GameOfLife.LifeGame.instance
{
    public partial class GameOfLifeInstance
    {
        public event NewMessageToClientHandler SendMessageToClient;

        public bool QuestionResponse(out string failureMessage, string playerId, string response)
        {
            failureMessage = null;
            if (!_waitingForPlayerResponse.HasValue) { failureMessage = "Got a player response, but no pending question"; return false; }

            switch (_waitingForPlayerResponse)
            {
                case Question.AutoInsurance:
                    if (response.ToLower().Trim() == "yes")
                    {
                        _myPlayers[playerId].HasAutoInsurance = true;
                        _myPlayers[playerId].BankBalance -= 1000;
                    }
                    _waitingForPlayerResponse = null;
                    break;
                case Question.FireInsurance:
                    if (response.ToLower().Trim() == "yes")
                    {
                        _myPlayers[playerId].HasFireInsurance = true;
                        _myPlayers[playerId].BankBalance -= 10000;
                    }
                    _waitingForPlayerResponse = null;
                    break;
                case Question.LifeInsurance:
                    if (response.ToLower().Trim() == "yes")
                    {
                        _myPlayers[playerId].HasLifeInsurance = true;
                        _myPlayers[playerId].BankBalance -= 10000;
                    }
                    _waitingForPlayerResponse = null;
                    break;
                case Question.CollegeOrBusiness:
                    _myPlayers[playerId].route_College = response.ToLower().Trim() == "yes";
                    _waitingForPlayerResponse = null;
                    break;
                case Question.BuyStock:
                    if (response.ToLower().Trim() == "yes")
                    {
                        _myPlayers[playerId].HasStock = true;
                        _myPlayers[playerId].BankBalance -= 50000;
                    }
                    _waitingForPlayerResponse = null;
                    break;
            }
            if (_myPlayers[playerId].IsTurn) //{ failureMessage = "Got a player response, but the response came from the wrong player"; return false; }
                while (!_waitingForPlayerResponse.HasValue && !MovePlayer(playerId)) { }

            return true;
        }

    }
}