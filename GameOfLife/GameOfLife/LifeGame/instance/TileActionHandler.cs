﻿namespace GameOfLife.LifeGame.instance
{
    public partial class GameOfLifeInstance
    {
        void HandleTileActions(Player currentPlayer, GameTileActionArgs e)
        {
            if (e is GameTileQuestionArgs)
            {
                var e2 = (GameTileQuestionArgs)e;
                switch (e2.QuestionType)
                {
                    case Question.AutoInsurance:
                        SendMessageToClient(null, new MessageToClientArgs
                        {
                            MessageType = ClientMessageType.Question,
                            QuestionText = "Do you want to buy Auto-Insurance for $1,000",
                            PlayerId = currentPlayer.PlayerAddress
                        });
                        _waitingForPlayerResponse = Question.AutoInsurance;
                        break;
                    case Question.CollegeOrBusiness:
                        SendMessageToClient(null, new MessageToClientArgs
                        {
                            MessageType = ClientMessageType.Question,
                            QuestionText = "Would you like to go the College route or the Business Route?",
                            PlayerId = currentPlayer.PlayerAddress
                        });
                        _waitingForPlayerResponse = Question.CollegeOrBusiness;
                        break;
                    case Question.LifeInsurance:
                        SendMessageToClient(null, new MessageToClientArgs
                        {
                            MessageType = ClientMessageType.Question,
                            QuestionText = "Do you want to buy Life-Insurance for $10,000",
                            PlayerId = currentPlayer.PlayerAddress
                        });
                        _waitingForPlayerResponse = Question.LifeInsurance;
                        break;
                    case Question.FireInsurance:
                        SendMessageToClient(null, new MessageToClientArgs
                        {
                            MessageType = ClientMessageType.Question,
                            QuestionText = "Do you want to buy Fire-Insurance for $10,000",
                            PlayerId = currentPlayer.PlayerAddress
                        });
                        _waitingForPlayerResponse = Question.FireInsurance;
                        break;
                    case Question.BuyStock:
                        SendMessageToClient(null, new MessageToClientArgs
                        {
                            MessageType = ClientMessageType.Question,
                            QuestionText = "Do you want to buy Stock for $50,000",
                            PlayerId = currentPlayer.PlayerAddress
                        });
                        _waitingForPlayerResponse = Question.BuyStock;
                        break;
                }
            }
            else if (e is PlayerBalanceChangeArgs)
            {
                var e2 = (PlayerBalanceChangeArgs)e;
                currentPlayer.BankBalance += e2.Amount;
            }
            else if (e is SpinAgainArgs)
            {
                spinAgain = true;
            }
            else if (e is SpinForGiftsArgs)
            {
                var spin = Random.Next(1, 11);
                int amount = 0;
                if (spin <= 3)
                    amount = 2000;
                else if (spin <= 6)
                    amount = 1000;
                foreach (var myPlayer in _myPlayers)
                {
                    if (myPlayer.Key != currentPlayer.PlayerAddress)
                        myPlayer.Value.BankBalance -= amount;
                    else
                    {
                        myPlayer.Value.BankBalance += amount * (_myPlayers.Count - 1);
                    }
                }
            }


            SendMessageToClient(null, new MessageToClientArgs
            {
                MessageType = ClientMessageType.PlayerUpdate,
                gameId = Gameid,
                PlayerId = currentPlayer.PlayerAddress,
                PlayerInfo = currentPlayer
            });
        }
    }
}