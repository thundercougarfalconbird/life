﻿using System;

namespace GameOfLife.LifeGame.signalr
{
    public interface IClientEventHandlers
    {
        void YourPlayerId(string playerId);
        void GameCreated(Guid gameId);

        void Game_PlayerJoined(Guid GameId, string PlayerId);
        void Game_Started(Guid GameId);
        void Game_Spinner_Spun(Guid GameId, int SpinTime, int Result);

        void Game_Player_Update(string PlayerId, int playerPostion, int playerid,
            int BankAmount, bool HasAutoInsurance, bool HasFireInsurance);

        void Game_YouJoinedAGame(Guid gameId);
        void Game_NewPlayersTurn(Guid playerId);

        void Player_Question(string QuestionText);
        void ErrorMessageFromServer(string message);
    }
}
