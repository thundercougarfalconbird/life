﻿using System;
using System.Threading.Tasks;
using GameOfLife.LifeGame;
using GameOfLife.LifeGame.signalr;
using Microsoft.AspNet.SignalR;

namespace GameOfLife
{
    public class LifeHub : Hub<IClientEventHandlers>
    {
        //put generic  SignalR specific stuff in here
        private readonly GameManager _gameManager;

        public LifeHub()
        {
            _gameManager = GameManager.Instance;
        }

        public bool CreateGame()
        {
            Guid gameId;
            string failureMessage;
            if (!_gameManager.CreateGame(out failureMessage, out gameId)) return SendErrorMessage(failureMessage);
            Clients.Caller.GameCreated(gameId);
            //TODO:  Remove before publishing.  Debug only code to make joining a game easier
            Clients.All.GameCreated(gameId);
            return true;
        }

        public async Task<bool> JoinGame(Guid GameId, string PlayerId)
        {
            string failureMessage;
            //Guid GameId = Guid.Parse(GameIdstring);
            if (!_gameManager.JoinGame(out failureMessage, GameId, PlayerId)) return SendErrorMessage(failureMessage);

            await Groups.Add(Context.ConnectionId, GameId.ToString());
            Clients.Group(GameId.ToString()).Game_PlayerJoined(GameId, PlayerId);
            Clients.Caller.Game_YouJoinedAGame(GameId);

            return true;
        }

        public bool StartGame(Guid GameId, string PlayerId)
        {
            string failureMessage;
            if (!_gameManager.StartGame(out failureMessage, GameId, PlayerId)) return SendErrorMessage(failureMessage);

            Clients.Group(GameId.ToString()).Game_Started(GameId);
            return true;
        }

        ///This "Spin" method is really "Player turn start Moving"
        public bool Spin(Guid GameId, string PlayerId)
        {
            string failureMessage;
            int spinningTime;
            int result;
            if (!_gameManager.Spin(out failureMessage, GameId, PlayerId, out spinningTime, out result)) return SendErrorMessage(failureMessage);
            Clients.Group(GameId.ToString()).Game_Spinner_Spun(GameId, spinningTime, result);

            return true;
        }

        public bool GetMyId()
        {
            Clients.Caller.YourPlayerId(Context.ConnectionId);
            return true;
        }

        public bool SendErrorMessage(string failureMessage)
        {
            Clients.Caller.ErrorMessageFromServer(failureMessage);
            return false;
        }

        public bool QuestionResponse(Guid GameId, string PlayerId, string Response)
        {
            string failureMessage;
            if (!_gameManager.QuestionResponse(out failureMessage, GameId, PlayerId, Response))
                return SendErrorMessage(failureMessage);
            return true;
        }

    }
}