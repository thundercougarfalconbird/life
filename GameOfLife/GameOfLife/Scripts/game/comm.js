﻿var hub;

var gameId;
var playerId;

$(function () {
    hub = $.connection.lifeHub;

    //Start the hub and wire up server call functions after it is started
    $.connection.hub.logging = true;//debugging

    hub.client.gameCreated = function (gameId) {
        //toastr.info("Game created with id of " + gameId);
        $("#gameIdTextBox").val(gameId);
    }
    hub.client.yourPlayerId = function (playerid) {
        playerId = playerid;
        //toastr.info("your player Id is " + playerId);
    }
    hub.client.errorMessageFromServer = function (message) {
        toastr.error(message);
    }
    hub.client.game_YouJoinedAGame = function (gameid) {
        gameId = gameid;
        //toastr.info("You joined a game with id of " + gameId + " you are a special person");
        game.screenManager.changeScreen("game");
        //game.playerNumber = 2;
    }
    hub.client.game_PlayerJoined = function (gameId, joiningPlayerId) {
        toastr.info("A player has joined your game " + gameId + " with the Id of " + joiningPlayerId);
        settings.numberOfPlayers++;
    }
    hub.client.game_Started = function (gameId) {
        toastr.info("The Game " + gameId + " has started");
        game.currentPlayer = 1;
        //game.playerNumber = 1;
    }
    hub.client.game_Spinner_Spun = function (gameId, spinTime, result) {
        toastr.error("The spinner has spun for " + spinTime + " seconds and resulted in a " + result);
    }
    hub.client.player_Question = function (questionText) {
        //toastr.info(questionText);
        var promptScreen = game.screenManager.get("prompt");
        promptScreen.displayText = questionText;
        promptScreen.acceptAction = function () { AcceptClick(); }
        promptScreen.declineAction = function () { DeclineClick(); }
        game.screenManager.changeScreen("prompt");
    }
    hub.client.game_Player_Update = function (playerId, playerPosition, playerint, BankAmount, CarInsurance, StockFireInsurance) {
        toastr.info("Have Player Update for Player " + playerint + " player tile = " + playerPosition);
        var previousPosition = eval("game.player" + game.currentPlayer + "TilePosition");
        eval("game.player" + game.currentPlayer + "PreviousTilePosition = " + previousPosition);
        eval("game.currentPlayer = " + playerint);
        eval("game.player" + playerint + "TilePosition = " + playerPosition);

        var serverId = hub.server.getMyId();
        if(serverId == playerId)
        {
            game.playerNumber = playerint;
        }
    }

    $.connection.hub
        .start()
        .done(function () {
            hub.server.getMyId();
            //hub.server.sendErrorMessage("This is a test message");
        });
});