﻿/////////////////////////////////
// Variables
/////////////////////////////////
var settings = new settings();
var game = null;


/////////////////////////////////
// Entry Point
/////////////////////////////////
function initializeGame() {
    WriteToConsole("InitializeGame()");

    game = new game();
    game.initialize();
    game.loadContent();
}

/////////////////////////////////
// Game
/////////////////////////////////
var game = function () {

    this.stage = null;
    this.renderer = null;
    this.ui = null;

    this.chanSpriteSheet = null;
    this.chanSprite = null;

    this.boardSprite = null;
    this.boardSpriteSheet = null;

    this.screenManager = null;

    this.currentPlayerText = null;

    this.currentPlayer = 0;
    this.playerNumber = 1;
    this.player1TilePosition = null;
    this.player2TilePosition = null;
    this.player3TilePosition = null;
    this.player4TilePosition = null;
    this.player5TilePosition = null;
    this.player6TilePosition = null;

    this.player1TilePreviousPosition = 0;
    this.player2TilePreviousPosition = 0;
    this.player3TilePreviousPosition = 0;
    this.player4TilePreviousPosition = 0;
    this.player5TilePreviousPosition = 0;
    this.player6TilePreviousPosition = 0;

    this.tokenSpriteSheet = null;
    this.player1TokenSprite = null;
    this.player2TokenSprite = null;
    this.player3TokenSprite = null;
    this.player4TokenSprite = null;
    this.player5TokenSprite = null;
    this.player6TokenSprite = null;

    /////////////////////////////////////////
    // Controls
    /////////////////////////////////////////
    this.btnCreateGame = null;
    this.btnJoinGame = null;
    this.btnStartGame = null;
    this.btnSpin = null;
    this.btnDance = null;

    this.btnAccept = null;
    this.btnDecline = null;

    this.textLines = ["", "", "", "", ""];
    this.textSpriteLine1 = null;
    this.textSpriteLine2 = null;
    this.textSpriteLine3 = null;
    this.textSpriteLine4 = null;
    this.textSpriteLine5 = null;

    /////////////////////////////////////////
    // *NOTE* ensure you are using game.* for game variables
    // this.* will keep referencing the browser window for some reason
    // This is only applicable in the game class
    /////////////////////////////////////////

    /////////////////////////////////////////
    // Main update loop of game
    /////////////////////////////////////////
    this.update = function () {
        try {
            game.ui.controls.forEach(function (control) {
                control.update();
            });
            game.screenManager.update();
            game.moveCamera();
            game.updatePlayers();
            game.updateHUD();
            game.stage.update();
        }
        catch (ex) {
            WriteToConsole(ex);
        }
    }

    this.moveCamera = function () {
        if (game.currentPlayer == 0) return;
        game.currentPlayerText.text = "Current Player: " + game.currentPlayer;
        var position = eval("game.player" + game.currentPlayer + "TilePosition");
        var previousPosition = eval("game.player" + game.currentPlayer + "PreviousTilePosition");
        if (position == null) return;
        var coord = settings.tileCoords[position];
        var pcoord = settings.tileCoords[previousPosition];
        var px = -pcoord[0];
        var py = -pcoord[1];
        var x = -coord[0] + 400;
        var y = -coord[1] + 300;

        var dx = x - px;
        var dy = y - py;

        createjs.Tween.get(game.boardSprite, { loop: false })
            .to({ x: x, y: y}, 1000, createjs.Ease.linear);
    };

    this.updatePlayers = function () {
        if(game.player1TokenSprite != null) game.player1TokenSprite.visible = false;
        if (game.player2TokenSprite != null) game.player2TokenSprite.visible = false;
        if (game.player3TokenSprite != null) game.player3TokenSprite.visible = false;
        if (game.player4TokenSprite != null) game.player4TokenSprite.visible = false;
        if (game.player5TokenSprite != null) game.player5TokenSprite.visible = false;
        if (game.player6TokenSprite != null) game.player6TokenSprite.visible = false;

        for (var i = 1; i <= settings.numberOfPlayers; i++)
        {
            var position = eval("game.player" + i + "TilePosition");
            if (position == null) continue;
            var coord = settings.tileCoords[position];
            var sprite = eval("game.player" + i + "TokenSprite");
            game.textLines[2] = coord;
            if (sprite != null) {
                sprite.visible = true;
                sprite.x = 400 - 32;
                sprite.y = 300 - 32;
                game.stage.update();
            }
        }
    };

    this.updateHUD = function () {
        game.textLines[0] = "You are player " + game.playerNumber;
        game.textLines[1] = "Player " + game.currentPlayer + "'s turn";

        if(game.textSpriteLine1 != null) game.textSpriteLine1.text = game.textLines[0];
        if(game.textSpriteLine2 != null) game.textSpriteLine2.text = game.textLines[1];
        if(game.textSpriteLine3 != null) game.textSpriteLine3.text = game.textLines[2];
        if(game.textSpriteLine4 != null) game.textSpriteLine4.text = game.textLines[3];
        if(game.textSpriteLine5 != null) game.textSpriteLine5.text = game.textLines[4];
    }

    /////////////////////////////////////////
    // Initializes the game
    /////////////////////////////////////////
    this.initialize = function () {
        game.ui = new ui();
        game.screenManager = new screenManager();

        game.stage = new createjs.Stage("gameCanvas");
        game.stage.enableMouseOver();

        createjs.Ticker.addEventListener("tick", game.update);
        createjs.Ticker.useRAF = true;
        createjs.Ticker.setFPS(60);

        game.screenManager.add("title", new titleScreen());
        game.screenManager.add("game", new gameScreen());
        game.screenManager.add("prompt", new promptScreen());
    }

    /////////////////////////////////////////
    // Load game content
    /////////////////////////////////////////
    this.loadContent = function () {
        game.ui.loadContent();

        game.currentPlayerText = new createjs.Text("Current Player: ", game.ui.fontName, "#ffffff");
        game.currentPlayerText.x = 0;
        game.currentPlayerText.y = 0;
        game.stage.addChild(game.currentPlayerText);

        game.screenManager.changeScreen("title");
    }
}

function Dance() {
    var sprite = new createjs.Sprite(game.chanSpriteSheet, "dance");
    sprite.framerate = 15;
    sprite.x = (Math.random() * 800) + 1;
    sprite.y = (Math.random() * 400) + 1;
    game.stage.addChild(sprite);
}