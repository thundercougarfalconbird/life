﻿/////////////////////////////////////////
// Base Screen
/////////////////////////////////////////
var screen = new function () {
    this.name = "";

    this.update = function () { }
    this.loadContent = function () { }
};

/////////////////////////////////////////
// Screen manager
/////////////////////////////////////////
function screenManager() {
    this.screens = [];
    this.screenNames = [];
    this.currentScreen = null;

    this.add = function (name, screen) {
        screen.name = name;
        this.screens[this.screens.length] = screen;
        this.screenNames[this.screenNames.length] = name;
    }    

    this.changeScreen = function(name) {
        var index = this.screenNames.indexOf(name);
        this.currentScreen = this.screens[index];
        game.stage.removeAllChildren();
        this.currentScreen.loadContent();
    }

    this.update = function () {
        this.currentScreen.update();
    }

    this.get = function (name) {
        var index = this.screenNames.indexOf(name);
        return this.screens[index];
    }
}

/////////////////////////////////////////
// Title Screen
/////////////////////////////////////////
function titleScreen() {
    this.name = "title";

    this.loadContent = function () {
        game.boardSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/realBoard.png"],
            frames: { width: 4096, height: 2048, count: 1, regX: 0, regY: 0}
        });
        game.boardSprite = new createjs.Sprite(game.boardSpriteSheet);
        game.boardSprite.x = 0;
        game.boardSprite.y = 0;
        game.stage.addChild(game.boardSprite);

        createjs.Tween.get(game.boardSprite, { loop: true })
            .to({ x: -2000 }, 3000, createjs.Ease.linear)
            .to({ x: -2500, y: -500 }, 1000, createjs.Ease.linear)
            .to({ x: -3000 }, 1000, createjs.Ease.linear)
            .to({ x: -3296, y: -1500 }, 2000, createjs.Ease.linear)
            .to({ x: -2500, y: -1448 }, 3000, createjs.Ease.linear)
            .to({ x: -1000, y: -1448 }, 2000, createjs.Ease.linear)
            .to({ x: -250, y: -500 }, 2000, createjs.Ease.linear)
            .to({ x: 0, y: 0 }, 2000, createjs.Ease.linear);

        /////////////////////////////////////////
        // Controls
        /////////////////////////////////////////
        game.btnCreateGame = new button(200, 450);
        game.btnCreateGame.text = "Create Game";
        game.btnCreateGame.action = function (evt) {
            CreateGame();
        };
        game.btnCreateGame.initialize();

        game.btnJoinGame = new button(400, 450);
        game.btnJoinGame.text = "Join Game";
        game.btnJoinGame.action = function (evt) {
            JoinGame();
        };
        game.btnJoinGame.initialize();           
    }

    this.update = function () {

    }
}

/////////////////////////////////////////
// Game Screen
/////////////////////////////////////////
function gameScreen() {
    this.name = "game";
    
    this.loadContent = function () {
        game.boardSprite = null;
        game.boardSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/realBoard.png"],
            frames: { width: 4096, height: 2048, count: 1, regX: 0, regY: 0 }
        });
        game.boardSprite = new createjs.Sprite(game.boardSpriteSheet);
        game.boardSprite.x = 0;
        game.boardSprite.y = 0;
        game.stage.addChild(game.boardSprite);

        game.btnDance = new button(600, 475);
        game.btnDance.text = "Dance!";
        game.btnDance.action = function (evt) {
            Dance();
        };
        game.btnDance.initialize();

        game.btnSpin = new button(400, 510);
        game.btnSpin.text = "Spin";
        game.btnSpin.action = function (evt) {
            Spin();
        };
        game.btnSpin.initialize();

        game.btnStartGame = new button(200, 510);
        game.btnStartGame.text = "Start Game";
        game.btnStartGame.action = function (evt) {
            StartGame();
        };
        game.btnStartGame.initialize();

        game.chanSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/FL_chan.png"],
            frames: { width: 110, height: 128, count: 80, regX: 0, regY: 0 },
            animations: { dance: [0, 79, "dance"] }
        });

        game.chanSprite = new createjs.Sprite(game.chanSpriteSheet, "dance");
        game.chanSprite.framerate = 15;
        game.chanSprite.x = 345;
        game.chanSprite.y = 236;
        //game.stage.addChild(game.chanSprite);

        game.tokenSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/LifeTokens.png"],
            frames: [
                [0,0,64,64],
                [64,0,64,64],
                [128,0,64,64],
                [192,0,64,64],
                [256,0,64,64],
                [320,0,64,64],
                [384,0,64,64],
                [448,0,64,64]
            ],
            animations: {
                red: 0,
                orange: 1,
                yellow: 2,
                green: 3,
                blue: 4,
                lightblue: 5,
                pink: 6,
                white: 7,
            }
        });

        var colorArray = ["red", "orange", "yellow", "green", "blue", "lightblue", "pink", "white"];
        colorArray = shuffle(colorArray);

        game.player1TokenSprite = new createjs.Sprite(game.tokenSpriteSheet, colorArray[0]);
        game.player1TokenSprite.x = 0;
        game.player1TokenSprite.y = 0;
        game.player1TokenSprite.visible = false;
        game.stage.addChild(game.player1TokenSprite);

        game.player2TokenSprite = new createjs.Sprite(game.tokenSpriteSheet, colorArray[1]);
        game.player2TokenSprite.x = 0;
        game.player2TokenSprite.y = 0;
        game.player2TokenSprite.visible = false;
        game.stage.addChild(game.player2TokenSprite);

        game.player3TokenSprite = new createjs.Sprite(game.tokenSpriteSheet, colorArray[2]);
        game.player3TokenSprite.x = 0;
        game.player3TokenSprite.y = 0;
        game.player3TokenSprite.visible = false;
        game.stage.addChild(game.player3TokenSprite);

        game.player4TokenSprite = new createjs.Sprite(game.tokenSpriteSheet, colorArray[3]);
        game.player4TokenSprite.x = 0;
        game.player4TokenSprite.y = 0;
        game.player4TokenSprite.visible = false;
        game.stage.addChild(game.player4TokenSprite);

        game.player5TokenSprite = new createjs.Sprite(game.tokenSpriteSheet, colorArray[4]);
        game.player5TokenSprite.x = 0;
        game.player5TokenSprite.y = 0;
        game.player5TokenSprite.visible = false;
        game.stage.addChild(game.player5TokenSprite);

        game.player6TokenSprite = new createjs.Sprite(game.tokenSpriteSheet, colorArray[5]);
        game.player6TokenSprite.x = 0;
        game.player6TokenSprite.y = 0;
        game.player6TokenSprite.visible = false;
        game.stage.addChild(game.player6TokenSprite);

        game.textSpriteLine1 = new createjs.Text(game.textLines[0], game.ui.fontName, "#ffffff");
        game.textSpriteLine1.x = 0;
        game.textSpriteLine1.y = 0;
        game.stage.addChild(game.textSpriteLine1);

        game.textSpriteLine2 = new createjs.Text(game.textLines[1], game.ui.fontName, "#ffffff");
        game.textSpriteLine2.x = 0;
        game.textSpriteLine2.y = 20;
        game.stage.addChild(game.textSpriteLine2);

        game.textSpriteLine3 = new createjs.Text(game.textLines[2], game.ui.fontName, "#ffffff");
        game.textSpriteLine3.x = 0;
        game.textSpriteLine3.y = 40;
        game.stage.addChild(game.textSpriteLine3);

        game.textSpriteLine4 = new createjs.Text(game.textLines[3], game.ui.fontName, "#ffffff");
        game.textSpriteLine4.x = 0;
        game.textSpriteLine4.y = 60;
        game.stage.addChild(game.textSpriteLine4);

        game.textSpriteLine5 = new createjs.Text(game.textLines[4], game.ui.fontName, "#ffffff");
        game.textSpriteLine5.x = 0;
        game.textSpriteLine5.y = 80;
        game.stage.addChild(game.textSpriteLine5);
        
    }

    this.update = function () {

    }
}

/////////////////////////////////////////
// Prompt Screen
/////////////////////////////////////////
function promptScreen() {
    this.name = "prompt";
    this.displayText = "Red Pill or Blue Pill?";
    this.result = false;
    this.id = -1;
    this.x = 100;
    this.y = 100;
    this.textOffsetX = 0;
    this.textOffsetY = 0;
    this.textColor = "#ffffff";
    this.acceptAction = null;
    this.declineAction = null;
    this.acceptText = "Accept";
    this.declineText = "Decline";

    this.textSprite = null;

    this.loadContent = function () {
        this.textSprite = new createjs.Text(this.displayText, game.ui.fontName, this.textColor);
        this.textSprite.x = this.x + this.textOffsetX;
        this.textSprite.y = this.y + this.textOffsetY;
        game.stage.addChild(this.textSprite);

        game.btnAccept = new button(200, 200);
        game.btnAccept.text = this.acceptText;
        game.btnAccept.action = function (evt) {
            var screen = game.screenManager.get("prompt");
            if (screen.acceptAction != null) screen.acceptAction();
        };
        game.btnAccept.initialize();

        game.btnDecline = new button(200, 250);
        game.btnDecline.text = this.declineText;
        game.btnDecline.action = function (evt) {
            var screen = game.screenManager.get("prompt");
            if (screen.declineAction != null) screen.declineAction();
        };
        game.btnDecline.initialize();
    }

    this.update = function () {

    }
}