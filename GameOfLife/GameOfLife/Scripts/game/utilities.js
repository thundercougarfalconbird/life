﻿/////////////////////////////////
// Utility Methods
/////////////////////////////////

function WriteToConsole(message) {
    var timestamp = new Date();
    console.log("[" +  timestamp + "] " + settings.title + ": " + message);
}

function shuffle(array) {
    var currentIndex = array.length;
    var tempValue = 0;
    var randomIndex = 0;

    while(0 != currentIndex)
    {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        tempValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = tempValue;
    }

    return array;
}